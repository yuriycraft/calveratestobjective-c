//
//  NetworkManager.m
//  calveraTestObjective-c
//
//  Created by book on 20.03.16.
//  Copyright © 2016 book. All rights reserved.
//

#import "AFNetworking.h"
#import "NSString+HTML.h"
#import "NetworkManager.h"
#import "NewsItemModel.h"
@interface NetworkManager ()

//@property (strong,nonatomic) AFHTTPSessionManager* requestOperationManager;

@end

@implementation NetworkManager

+ (NetworkManager *)sharedManager {

  static NetworkManager *manager = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    manager = [[NetworkManager alloc] init];
  });
  return manager;
}

- (void)getNewsFromServer:(void (^)(NSArray *array))success
                onFailure:(void (^)(NSString *error))failure {
  NSMutableArray *result = [NSMutableArray array];
  NSString *urlString = @"http://calvera.su/5839.json";

  NSURL *URL = [NSURL URLWithString:urlString];
  AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
  [manager setResponseSerializer:[AFHTTPResponseSerializer serializer]];
  manager.requestSerializer = [AFHTTPRequestSerializer serializer];
  [manager.requestSerializer setValue:@"text/html"
                   forHTTPHeaderField:@"Content-Type"];
  [manager.requestSerializer setValue:@"1" forHTTPHeaderField:@"DNT"];

  [manager GET:URL.absoluteString
      parameters:nil
      progress:nil
      success:^(NSURLSessionTask *task, id responseObject) {

        if ([responseObject isKindOfClass:[NSData class]] &&
            ((NSData *)responseObject).length > 0) {
          NSError *error = nil;
          NSString *string =
              [[NSString alloc] initWithData:responseObject
                                    encoding:NSWindowsCP1251StringEncoding];
          NSString *str = [string stringByConvertingHTMLToPlainText];
          NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
          NSDictionary *jsonObj =
              [NSJSONSerialization JSONObjectWithData:data
                                              options:kNilOptions
                                                error:&error];
          BOOL isValid = [NSJSONSerialization isValidJSONObject:jsonObj];

          if (isValid) {
            NSArray *content = [jsonObj objectForKey:@"news"];
            for (NSDictionary *dict in content) {
              NewsItemModel *item =
                  [[NewsItemModel alloc] initWithServerResponse:dict];
#warning only 208
              if ([item.category_id isEqual:@208]) {
                [result addObject:item];
              }
            }
            success(result);
          }

        } else {
          failure(nil);
          NSLog(@"ELJSON: %@", responseObject);
        }

      }
      failure:^(NSURLSessionTask *operation, NSError *error) {
        failure(error.description);
      }];
}
@end
