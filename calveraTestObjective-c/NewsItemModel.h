//
//  NewsItemModel.h
//  calveraTestObjective-c
//
//  Created by book on 20.03.16.
//  Copyright © 2016 book. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsItemModel : NSObject  {
    
    NSNumber *category_id;
    NSString *classNews;
    NSNumber *comment_count;
    NSString *content;
    NSString *news_id;
    NSString *link;
    NSNumber *main;
    NSString *nick;
    NSDate *posted_time;
    NSString *title;
}

@property(nonatomic, strong) NSNumber *category_id;
@property(nonatomic, strong) NSString *classNews;
@property(nonatomic, strong) NSNumber *comment_count;
@property(nonatomic, strong) NSString *content;
@property(nonatomic, strong) NSString *news_id;
@property(nonatomic, strong) NSString *link;
@property(nonatomic, strong) NSNumber *main;
@property(nonatomic, strong) NSString *nick;
@property(nonatomic, strong) NSDate *posted_time;
@property(nonatomic, strong) NSString *title;
-(id)initWithServerResponse :(NSDictionary*) responseObject;
@end
