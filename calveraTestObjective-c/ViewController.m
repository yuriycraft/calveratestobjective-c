//
//  ViewController.m
//  calveraTestObjective-c
//
//  Created by book on 20.03.16.
//  Copyright © 2016 book. All rights reserved.
//


#import "NetworkManager.h"
#import "NewsItemModel.h"
#import "PublicationTableViewCell.h"
#import "ViewController.h"

static NSString *kPublicationCellId = @"publicationCell";

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property(weak, nonatomic) IBOutlet UITableView *tableView;
@property(assign, nonatomic) BOOL sorted;
@end

@implementation ViewController {
  NSArray *_content;
  UIRefreshControl *_refreshControl;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  _sorted = NO;

  [_tableView setScrollsToTop:YES];
  self.automaticallyAdjustsScrollViewInsets = NO;
  _tableView.rowHeight = UITableViewAutomaticDimension;
  _tableView.estimatedRowHeight = 100.f;

  [_tableView registerNib:[UINib nibWithNibName:@"PublicationTableViewCell"
                                         bundle:nil]
      forCellReuseIdentifier:kPublicationCellId];

  _refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectZero];

  [_tableView addSubview:_refreshControl];

  [_refreshControl addTarget:self
                      action:@selector(refreshAction)
            forControlEvents:UIControlEventValueChanged];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];

  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    [_refreshControl beginRefreshing];
    [_tableView
        setContentOffset:CGPointMake(0, _tableView.contentOffset.y -
                                            _refreshControl.frame.size.height)
                animated:YES];
    [self refreshAction];
  });
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void)refreshAction {

  [[NetworkManager sharedManager] getNewsFromServer:^(NSArray *array) {
    _content = array;
    [_refreshControl endRefreshing];
    [_tableView reloadData];
  }
      onFailure:^(NSString *error){

      }];
}
#pragma mark - Table Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section

{

  return [_content count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  NewsItemModel *item = [_content objectAtIndex:indexPath.row];

  PublicationTableViewCell *cell =
      [self.tableView dequeueReusableCellWithIdentifier:kPublicationCellId];

  if (cell == nil) {

    cell = [[PublicationTableViewCell alloc]
          initWithStyle:UITableViewCellStyleDefault
        reuseIdentifier:kPublicationCellId];
  }

  if (item) {

    if ([item.category_id isEqual:@208]) {
      cell.categoryLabel.text = @"Футбол";
    } else {
      cell.categoryLabel.text = @"";
    }

    if ([item.main isEqual:@1]) {
      cell.publicationLabel.font = [UIFont boldSystemFontOfSize:17];
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    cell.timeAgoLabel.text = [formatter stringFromDate:item.posted_time];

    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image = [UIImage imageNamed:@"comments_badge"];
    NSString *stringHeadline = item.title;
    NSAttributedString *attachmentLock =
        [NSAttributedString attributedStringWithAttachment:attachment];
    NSAttributedString *myText = [[NSMutableAttributedString alloc]
        initWithString:[NSString stringWithFormat:@"%@  ", stringHeadline]];
    NSMutableAttributedString *lockString =
        [[NSMutableAttributedString alloc] initWithAttributedString:myText];

    [lockString appendAttributedString:attachmentLock];

    [lockString
        appendAttributedString:
            [[NSAttributedString alloc]
                initWithString:[NSString
                                   stringWithFormat:@" %@",
                                                    [item.comment_count
                                                            stringValue]]]];
    cell.publicationLabel.attributedText = lockString;
    [cell.publicationLabel sizeToFit];
    [cell.publicationLabel sizeToFit];
  }
  return cell;
}

#pragma mark - Actions

- (IBAction)sortBarButtonItemAction:(id)sender {

  NSSortDescriptor *dateDescriptor;
  if (_sorted) {
    _sorted = NO;
    dateDescriptor =
        [NSSortDescriptor sortDescriptorWithKey:@"posted_time" ascending:NO];

  } else {
    _sorted = YES;
    dateDescriptor =
        [NSSortDescriptor sortDescriptorWithKey:@"posted_time" ascending:YES];
  }
  NSArray *sortDescriptors = [NSArray arrayWithObject:dateDescriptor];
  NSArray *sortedEventArray =
      [_content sortedArrayUsingDescriptors:sortDescriptors];
  _content = sortedEventArray;
  [_tableView reloadData];
}
@end
