//
//  NetworkManager.h
//  calveraTestObjective-c
//
//  Created by book on 20.03.16.
//  Copyright © 2016 book. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject
+(NetworkManager*)sharedManager;
-(void)getNewsFromServer:(void(^)(NSArray *array)) success
               onFailure:(void(^)(NSString* error)) failure;
@end
