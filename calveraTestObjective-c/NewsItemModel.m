//
//  NewsItemModel.m
//  calveraTestObjective-c
//
//  Created by book on 20.03.16.
//  Copyright © 2016 book. All rights reserved.
//

#import "NewsItemModel.h"

@implementation NewsItemModel
@synthesize  category_id,classNews, comment_count, content, news_id, link, main,
nick, posted_time, title;

#pragma mark init

-(id)initWithServerResponse :(NSDictionary*) responseObject{
    self = [super init];
    if (self) {
        category_id = [responseObject objectForKey:@"category_id"];
        ////        classNews = [decoder decodeObjectForKey:@"class"];
        comment_count = [responseObject objectForKey:@"comment_count"];
        ////        content = [decoder decodeObjectForKey:@"content"];
        ////        news_id = [decoder decodeObjectForKey:@"id"];
        ////        link = [responseObject objectForKey:@"link"];
        main = [responseObject objectForKey:@"main"];
        ////        nick = [decoder decodeObjectForKey:@"nick"];
        NSNumber *startTime = [responseObject objectForKey:@"posted_time"];
        posted_time = [NSDate dateWithTimeIntervalSince1970:[startTime doubleValue]];
        title = [responseObject objectForKey:@"title"];
    }
    return self;
}

@end
