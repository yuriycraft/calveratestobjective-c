//
//  PublicationTableViewCell.h
//  calveraTestObjective-c
//
//  Created by book on 20.03.16.
//  Copyright © 2016 book. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PublicationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeAgoLabel;
@property (weak, nonatomic) IBOutlet UILabel *publicationLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;

@end
